package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
)

func main() {
	numbPtr := flag.Int("k", 5, "Number of routines")
	flag.Parse()
	urls := make(chan string)
	total := make(chan int)
	go getUrls(urls)
	go processUrl(urls, total, *numbPtr)

	printTotal(total)

}

func getUrls(urls chan<- string) {
	defer close(urls)
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		urls <- scanner.Text()
	}
}

func processUrl(urls <-chan string, total chan<- int, k int) {
	q := make(chan int, k)
	defer close(total)
	defer close(q)
	var wg sync.WaitGroup
	for url := range urls {
		wg.Add(1)
		q <- 1
		go func(url string) {
			defer wg.Done()
			resp, err := http.Get(url)
			if err != nil {
				fmt.Printf("Error request, url: %s; err: %s\n", url, err)
				return
			}
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				fmt.Printf("Error read resp body, err: %s\n", err)
				return
			}
			content := string(body)

			count := strings.Count(content, "Go")
			total <- count
			fmt.Printf("Count for %s: %d\n", url, count)
			if resp != nil {
				resp.Body.Close()
			}
			<-q
		}(url)
	}
	wg.Wait()
}

func printTotal(totalChan <-chan int) {
	total := 0
	for v := range totalChan {
		total += v
	}
	fmt.Printf("Total: %d\n", total)
}
